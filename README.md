# eCommerce

To run the client and server folders, please run for each:

### `npm install`

### `npm start`

Client dependencies:
"dependencies": {
	"@fortawesome/fontawesome-svg-core": "^1.2.22",
	"@fortawesome/free-brands-svg-icons": "^5.10.2",
	"@fortawesome/free-regular-svg-icons": "^5.10.2",
	"@fortawesome/free-solid-svg-icons": "^5.10.2",
	"@fortawesome/react-fontawesome": "^0.1.4",
	"axios": "^0.19.0",
	"cors": "^2.8.5",
	"react": "^16.9.0",
	"react-dom": "^16.9.0",
	"react-router": "^5.0.1",
	"react-router-dom": "^5.0.1",
	"react-scripts": "3.1.1",
	"react-stripe-checkout": "^2.6.3",
	"react-stripe-elements": "^5.0.0"
}

Server dependencies:
"dependencies": {
	"bcrypt": "^3.0.6",
	"bcrypt-nodejs": "0.0.3",
	"body-parser": "^1.19.0",
	"cors": "^2.8.5",
	"express": "^4.17.1",
	"jsonwebtoken": "^8.5.1",
	"mongoose": "^5.6.10",
    "nodemailer": "^6.3.0",
    "stripe": "^7.8.0"

